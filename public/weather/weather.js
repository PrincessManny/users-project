async function getWeather() {
    const weatherDescription = document.querySelector('.weather-description');
    const temperature = document.querySelector('.temperature');
    const city = document.querySelector('.City').getElementsByTagName('span')[0];

    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city.textContent}&appid=4c1a4439778eb3eca88d7a1d8d5069c7&units=metric&lang=ru`;
    const res = await fetch(url);
    const data = await res.json();
    
    temperature.textContent = `${data.main.temp}°C`;
    weatherDescription.textContent = data.weather[0].description;
  }  

  document.addEventListener('DOMContentLoaded', getWeather);