<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Service\XlsxService;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;


class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureActions(Actions $actions): Actions
    {
    
        $exportAction = Action::new('export', 'Экспорт', 'fa fa-download')
        ->displayAsLink()
        ->linkToCrudAction('export')
        ->createAsGlobalAction()
        ;
    
        return $actions
        ->add(Crud::PAGE_INDEX, $exportAction)
        ->add(Crud::PAGE_INDEX, Action::DETAIL)

        ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
            return $action->setIcon('fa-solid fa-plus')->setLabel('Добавить пользователя');
        })
    ;
    }

    public function export (XlsxService $xlsxService, AdminContext $context)
    {
        $title = 'users';

        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));

        $fieldNames = [];
        foreach($fields as $field) {
            $fieldNames[] = $field->getProperty();
        }

        $filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $queryBuilder = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters);
        
        $result = $queryBuilder->getQuery()->getArrayResult();

        return $xlsxService->generate($title, $fieldNames, $result);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return Crud::new()
        ->overrideTemplate('crud/detail', 'weather.html.twig')
        ->setPageTitle('index', 'Пользователи')
        ;
    }
}
