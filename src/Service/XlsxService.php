<?php
 
namespace App\Service;
 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
 
class XlsxService extends AbstractController
{
	public function generate($title, $headers, $arrayData)
	{
		$spreadsheet = new Spreadsheet();
 
		$fieldNames = $spreadsheet->getActiveSheet()->fromArray($headers);
		$sheet = $spreadsheet->getActiveSheet()->fromArray($arrayData, null, 'A2');
		$sheet->setTitle($title);
 
		$writer = new Xlsx($spreadsheet);
 
		$fileName = "$title.xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), $fileName);
 
		$writer->save($temp_file);

		return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
	}
}