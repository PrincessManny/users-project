Symfony powered CRUD web-application

## *Features*
- Show, edit and delete users (esp. if you don't like them)
- Discover real-time weather in user's city via OpenWeatherMap
- Export users information in .xlsx

# Installation
## Requirements
  - **Symfony** 6.3
  - **PHP** 8.1
  - **NodeJS** 16.20
  - **MySQL** 8.0

Install symfony dependencies via composer
```
composer install
```

Add DB connection string to .env file.

Make migrations with Doctrine
```
php bin/console doctrine:migrations:migrate
```

Add entrypoint to your http server of choice

    public/index.php

## *What we are to do*
- Add cashing for weather
- Improve the project with Docker